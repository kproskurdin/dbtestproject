﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Migrations.DataLayer
{
    public class Ef6DataContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}
