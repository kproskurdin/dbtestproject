# These variables should be set via the Octopus web portal:
#
#   ConnectionString         - The .Net connection string for the DB


Write-Host "Connection String: <"$ConnectionString">"

# Get the exe name based on the directory
$contentPath  = $OctopusOriginalPackageDirectoryPath
$fullPath = (Join-Path $contentPath "migrate.exe")
Write-Host "Migrate Path:" $fullPath

cd $contentPath
write-host "Working Dir: "$(get-location)

# Run the migration utility
& ".\migrate.exe" EF_Migrations.exe EF_Migrations.Migrations.Configuration /startupConfigurationFile:EF_Migrations.exe.config /verbose | Write-Host